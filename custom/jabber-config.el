(defun async-jabber-sound-notification (file)
  (start-process-shell-command "jabber-notification" nil (format "mpv --vid=no %s" file)))

(defun alt-async-jabber-sound-notification (file)
  (require 'async)
  (async-start `(lambda () (set 'file ,file) (play-sound-file file))))

(use-package jabber
  :defer  t
  :ensure t

  :init
  (setq jabber-account-list `(("chedi.toueiti@gmail.com"
                             (:port            . 5223                                 )
                             (:network-server  . "talk.google.com"                    )
                             (:connection-type . ssl                                  )
                             (:password        . ,(secrets-get-secret "emacs" "gtalk")))))

  (setq jabber-alert-info-message-hooks '(jabber-info-wave
                                          jabber-info-echo
                                          jabber-info-display))

  (setq jabber-alert-message-hooks '(jabber-message-echo
                                     jabber-message-wave
                                     jabber-message-scroll
                                     jabber-message-switch
                                     jabber-message-display))

  (setq jabber-play-sound-file    'async-jabber-sound-notification)
  (setq jabber-alert-message-wave "~/.emacs.d/Spirit.wav")
  (setq jabber-roster-show-title  nil))

(provide 'jabber-config)
