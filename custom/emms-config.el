(use-package emms
  :defer  t
  :ensure t

  :init
  (setq emms-player-mpv-parameters               '("--quiet" "--really-quiet" "--vid=no"))
  (setq emms-source-file-default-directory       "~/Music/"                              )
  (setq emms-source-file-directory-tree-function #'emms-source-file-directory-tree-find  )
  (setq emms-track-description-function
        (lambda (track)
          (let ((artist (emms-track-get track 'info-artist))
                (album  (emms-track-get track 'info-album))
                (number (emms-track-get track 'info-tracknumber))
                (title  (emms-track-get track 'info-title)))
            (if (and artist album title)
                (if number (format "%s - %s" artist title)
                  (format "%s: %s - %s" artist title))
              (emms-track-simple-description track)))))

  :bind (("<C-f1>"          . emms-smart-browse)
         ("C-à"             . emms-volume-raise)
         ("C-ç"             . emms-volume-lower)
         ("<XF86AudioStop>" . emms-stop        )
         ("<XF86AudioNext>" . emms-next        )
         ("<XF86AudioPlay>" . emms-pause       )
         ("<XF86AudioPrev>" . emms-previous    ))

  :config
  (add-to-list 'emms-player-list 'emms-player-mpv)

  (emms-standard       )
  (emms-mode-line     1)
  (emms-playing-time  1)
  (emms-default-players)

  (require 'dbus)

  (dbus-register-method
   :session "org.mpris.MediaPlayer2.emms" "/org/mpris/MediaPlayer2"
   "org.mpris.MediaPlayer2.Player" "PlayPause" 'emms-pause)

  (dbus-register-method
   :session "org.mpris.MediaPlayer2.emms" "/org/mpris/MediaPlayer2"
   "org.mpris.MediaPlayer2.Player" "Next" 'emms-next)

  (dbus-register-method
   :session "org.mpris.MediaPlayer2.emms" "/org/mpris/MediaPlayer2"
   "org.mpris.MediaPlayer2.Player" "Previous" 'emms-previous)

  (dbus-register-method
   :session "org.mpris.MediaPlayer2.emms" "/org/mpris/MediaPlayer2"
   "org.mpris.MediaPlayer2.Player" "Stop" 'emms-stop))

(provide 'emms-config)
