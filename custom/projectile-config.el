(defun advice-projectile-no-sub-project-files ()
  (projectile-files-via-ext-command (projectile-get-ext-command)))


(defun project-next-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'next-buffer t))

(defun project-previous-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'previous-buffer t))

(defun next-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'next-buffer nil))

(defun previous-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'previous-buffer nil))

(defun move-to-project-code-buffer (move-handler restrict-to-project)
  (require 'projectile)
  (let ((original_buffer_name (buffer-name))
        (exausted_buffers nil)
        (project_buffers_names
         (when (projectile-project-p)
           (mapcar 'buffer-name (projectile-project-buffers-non-visible)))
         ))
    (when (or project_buffers_names (not restrict-to-project))
      (funcall move-handler)
      (while (and (not exausted_buffers)
                  (or (string-match-p "^\*" (buffer-name))
                      (and project_buffers_names restrict-to-project
                           (not (member (buffer-name) project_buffers_names)))))
        (if (not (equal original_buffer_name (buffer-name)))
            (funcall move-handler)
          (setq exausted_buffers t))))))

(use-package projectile
  :ensure t

  :init
  (use-package helm-ag
    :ensure t)
  (use-package grep
    :ensure t)

  :bind (
         ("<M-f4>"   . helm-projectile-grep)
         ("<s-end>"  . next-code-buffer    )
         ("<s-home>" . previous-code-buffer))

  :config
  (use-package helm-projectile
    :ensure t

    :bind (
           ("s-²" . helm-projectile))

    :config
    (helm-projectile-on)
    (key-chord-define-global "²²" 'helm-projectile   )
    (key-chord-define-global "zz" 'helm-projectile-ag))

  (setq projectile-enable-caching               nil                                                    )
  (setq projectile-require-project-root         nil                                                    )
  (setq projectile-project-root-files-bottom-up (delete ".git" projectile-project-root-files-bottom-up))

  (setq projectile-project-root-files-functions
      '(projectile-root-local
        projectile-root-top-down
        projectile-root-bottom-up))

  (global-set-key [remap next-buffer    ] 'project-next-code-buffer    )
  (global-set-key [remap previous-buffer] 'project-previous-code-buffer)

  (add-to-list 'projectile-project-root-files ".git")
  (advice-add 'projectile-get-repo-files :override #'advice-projectile-no-sub-project-files)
  (projectile-global-mode))

(provide 'projectile-config)
