(provide 'custom-tramp-config)

(defun custom-tramp-config ()
  (setq-default tramp-default-method "ssh")

  ;;(add-to-list 'tramp-default-proxies-alist '(nil          "\\`root\\'" "/ssh:%h:"))
  ;;(add-to-list 'tramp-default-proxies-alist '("localhost"                  nil nil))
  ;;(add-to-list 'tramp-default-proxies-alist '((regexp-quote (system-name)) nil nil))
  ;;(add-to-list 'tramp-default-proxies-alist '("sal7oufa"    nil "/sudo:localhost:"))


  (defun th-rename-tramp-buffer ()
    (when (file-remote-p (buffer-file-name))
      (rename-buffer (format "%s:%s"
                             (file-remote-p (buffer-file-name) 'method)
                             (buffer-name)))))

  (add-hook 'find-file-hook 'th-rename-tramp-buffer)

  (defadvice find-file (around th-find-file activate)
    "Open FILENAME using tramp's sudo method if it's read-only."
    (if (and (not (file-writable-p (ad-get-arg 0)))
             (y-or-n-p (concat "File " (ad-get-arg 0) " is read-only.  Open it as root? ")))
        (th-find-file-sudo (ad-get-arg 0))
      ad-do-it))

  (defun th-find-file-sudo (file)
    "Opens FILE with root privileges."
    (interactive "F")
    (set-buffer (find-file (concat "/sudo::" file))))
)
