(use-package ansible
  :ensure t

  :config
  (company-mode 1)
  (ansible::add-font-lock)

  (add-hook 'yaml-mode-hook #'smartparens-mode)
  (add-hook 'yaml-mode-hook '(lambda () (ansible 1)))

  (add-to-list 'auto-mode-alist '("\\.j2\\'" . jinja2-mode))

  (use-package ansible-doc
    :ensure t

    :bind (
           :map ansible::key-map
           ("C-c C-d" . ansible-doc))

    :config
    (add-hook 'yaml-mode-hook #'ansible-doc-mode)))

(provide 'ansible-config)
