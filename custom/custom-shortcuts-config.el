(provide 'custom-shortcuts-config)

(defun custom-shortcuts-config ()
  (require 'cua-base)

  (key-chord-define-global "éé" 'mark-whole-buffer        )
  (key-chord-define-global "xs" 'save-buffer              )
  (key-chord-define-global "²&" 'helm-find-files          )
  (key-chord-define-global "&é" 'helm-mini                )
  (key-chord-define-global "&&" 'helm-locate              )
  (key-chord-define-global "ùm" 'rgrep                    )
  (key-chord-define-global "<w" 'helm-dash-activate-docset)
  (key-chord-define-global "wx" 'helm-dash                )

  (key-chord-define-global "$$" 'save-buffers-kill-emacs )
  (key-chord-define-global "ùù" 'er/expand-region        )
  (key-chord-define-global "**" 'mark-all-like-this      )
  (key-chord-define-global "gt" 'goto-line               )
  (key-chord-define-global "àà" 'find-temp-file          )
  (key-chord-define-global "aa" 'align-current           )
  (key-chord-define-global "qq" 'magit-status            )

  (key-chord-define lisp-mode-map "cd" 'eval-last-sexp)

  (global-set-key (kbd "<f2>"  ) 'webjump             )
  (global-set-key (kbd "<f6>"  ) 'findr-query-replace )
  (global-set-key (kbd "<C-f2>") 'browse-url          )
  (global-set-key (kbd "<C-f5>") 'revert-buffer       )

  (global-set-key (kbd "<f5>") 'google-this)

  (global-set-key (kbd "C-:"      ) 'toggle-comment-on-line)
  (global-set-key (kbd "C-!"      ) 'kill-current-buffer   )
  (global-set-key (kbd "<M-f4>"   ) 'kill-current-buffer   )
  (global-set-key (kbd "<s-next>" ) 'next-buffer           )
  (global-set-key (kbd "<s-prior>") 'previous-buffer       )

  (global-set-key (kbd "<M-f3>"    ) 'mc/mark-all-like-this     )
  (global-set-key (kbd "C-S-<down>") 'mc/mark-next-like-this    )
  (global-set-key (kbd "C-S-<up>"  ) 'mc/mark-previous-like-this)

  (global-set-key (kbd "s-<up>"        ) 'windmove-up   )
  (global-set-key (kbd "s-<left>"      ) 'windmove-left )
  (global-set-key (kbd "s-<down>"      ) 'windmove-down )
  (global-set-key (kbd "s-<right>"     ) 'windmove-right)
  (global-set-key (kbd "<C-M-s-home>"  ) 'buf-move-up   )
  (global-set-key (kbd "<C-M-s-end>"   ) 'buf-move-down )
  (global-set-key (kbd "<C-M-s-next>"  ) 'buf-move-right)
  (global-set-key (kbd "<C-M-s-delete>") 'buf-move-left )

  (global-set-key (kbd "<s-f4>") 'delete-window       )
  (global-set-key (kbd "<s-f3>") 'split-window-below  )
  (global-set-key (kbd "<s-f2>") 'split-window-right  )
  (global-set-key (kbd "<s-f1>") 'delete-other-windows)

  (global-set-key (kbd "<C-M-s-up>"   ) 'enlarge-window             )
  (global-set-key (kbd "<C-M-s-down>" ) 'shrink-window              )
  (global-set-key (kbd "<C-M-s-left>" ) 'enlarge-window-horizontally)
  (global-set-key (kbd "<C-M-s-right>") 'shrink-window-horizontally )

  (global-set-key (kbd "C-M-s-!") 'eyebrowse-close-window-config)
  (global-set-key (kbd "C-M-s-,") 'eyebrowse-next-window-config )
  (global-set-key (kbd "C-M-s-;") 'eyebrowse-prev-window-config )

  (global-set-key (kbd "M-²"       ) 'eyebrowse-switch-to-window-config)
  (global-set-key (kbd "M-&"       ) 'eyebrowse-create-window-config   )
  (global-set-key (kbd "M-é"       ) 'avy-goto-line                    )
  (global-set-key (kbd "C-é"       ) 'linum-mode                       )
  (global-set-key (kbd "<C-escape>") 'eyebrowse-rename-window-config   )
  (global-set-key (kbd "<s-insert>") 'eyebrowse-next-window-config     )
  (global-set-key (kbd "<s-delete>") 'eyebrowse-prev-window-config     )

  (global-set-key (kbd "<f8>") 'neotree-toggle)

  (global-set-key (kbd "C-+"              ) 'text-scale-increase)
  (global-set-key (kbd "<C-s-kp-add>"     ) 'text-scale-increase)
  (global-set-key (kbd "C--"              ) 'text-scale-decrease)
  (global-set-key (kbd "<C-s-kp-subtract>") 'text-scale-decrease)

  (global-set-key (kbd "C-§"  ) 'kill-current-buffer        )
  (global-set-key (kbd "C-s-b") 'kill-emacs                 )
  (global-set-key (kbd "C-µ"  ) 'delete-this-buffer-and-file)

  (global-set-key (kbd "C-s-t") 'find-temp-file)

  (global-set-key (kbd "<M-up>"    ) 'move-line-up                    )
  (global-set-key (kbd "<M-down>"  ) 'move-line-down                  )
  (global-set-key (kbd "<C-s-up>"  ) 'move-text-up                    )
  (global-set-key (kbd "<C-s-down>") 'move-text-down                  )
  (global-set-key (kbd "C-<"       ) 'toggle-truncate-lines           )
  (global-set-key (kbd "<C-M-tab>" ) 'insert-tab                      )
  (global-set-key (kbd "s-d"       ) 'duplicate-current-line-or-region)
  (global-set-key (kbd "M-y"       ) 'helm-show-kill-ring             )

  (global-set-key (kbd "C-s-o") 'find-file-in-project       )
  (global-set-key (kbd "C-s-s") 'desktop-save-in-desktop-dir)

  (global-set-key (kbd "<C-f3>") 'rgrep               )
  (global-set-key (kbd "<C-f4>") 'find-name-dired     )
  (global-set-key (kbd "C-;"   ) 'pop-tag-mark        )

  (global-set-key (kbd "M-X"        ) 'smex-major-mode-commands)
  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

  (global-set-key (kbd "<M-s-right>") 'sp-forward-symbol )
  (global-set-key (kbd "<M-s-left>" ) 'sp-backward-symbol)

  (global-set-key (kbd "C-¶") 'rename-file-and-buffer  )
  (global-set-key (kbd "C-ŧ") 'insert-current-timestamp)

  (global-set-key (kbd "<C-s-tab>") 'tabify              )
  (global-set-key (kbd "C-M-="    ) 'indent-region       )
  (global-set-key (kbd "<M-s-tab>") 'hs-toggle-hiding    )
  (global-set-key [escape]          'keyboard-escape-quit)
  (global-set-key (kbd "<M-f1>"   ) (lambda () (interactive) (switch-to-buffer "*Messages*")))
  (global-set-key (kbd "<M-f2>"   ) (lambda () (interactive) (switch-to-buffer "*scratch*" )))

  (global-set-key (kbd "C-a"            ) 'move-beginning-of-line)
  (global-set-key (kbd "<C-S-backspace>") 'kill-whole-line       )
  (global-set-key (kbd "<M-return>"     ) (lambda() (interactive) (previous-line) (electric-newline-and-maybe-indent)))
  (global-set-key (kbd "<S-return>"     ) (lambda() (interactive) (move-end-of-line nil) (electric-newline-and-maybe-indent)))
  (global-set-key (kbd "<C-s-M-return>") 'magit-diff-buffer-file)

  (define-key cua-global-keymap (kbd "<C-return>") 'nil)
  (define-key cua-global-keymap (kbd "<C-escape>") 'cua-set-rectangle-mark)

  (global-set-key (kbd "<C-S-prior>") 'move-line-up       )
  (global-set-key (kbd "<C-S-next>" ) 'move-line-down     )
  (global-set-key (kbd "<C-s-next>" ) 'yas/next-field     )
  (global-set-key (kbd "<C-s-prior>") 'yas/prev-field     )
  (global-set-key (kbd "<M-s-next>" ) 'narrow-to-region   )
  (global-set-key (kbd "<M-s-prior>") 'narrow-to-page     )

  (global-set-key (kbd "<C-s-return>") 'auto-insert)

  (global-set-key (kbd "C-M-q") 'emoji-cheat-sheet-plus-insert)

  (global-set-key (kbd "C-c C-u") 'string-inflection-all-cycle)

  (global-set-key (kbd "C-z"  ) 'undo-tree-undo     )
  (global-set-key (kbd "C-S-z") 'undo-tree-redo     )
  (global-set-key (kbd "C-M-z") 'undo-tree-visualize))

(global-set-key (kbd "<f9>"  ) 'edebug-defun )
(global-set-key (kbd "<C-S-kp-subtract>") 'mc/unmark-next-like-this)
