(provide 'custom-eshell-config)

(defun custom-eshell-config ()

  (eval-after-load 'esh-opt
    '(progn
       (require 'em-cmpl  )
       (require 'em-term  )
       (require 'em-smart )
       (require 'em-prompt)
       (setenv "PAGER" "cat")
       (add-hook 'eshell-mode-hook '(lambda () (define-key eshell-mode-map "\C-a" 'eshell-bol)))
       (add-to-list 'eshell-visual-commands "ssh")
       (add-to-list 'eshell-visual-commands "tail")
       (add-to-list 'eshell-visual-commands "htop")
       (add-to-list 'eshell-command-completions-alist '("gunzip" "gz\\'"))
       (add-to-list 'eshell-command-completions-alist '("tar" "\\(\\.tar|\\.tgz\\|\\.tar\\.gz\\)\\'"))
       ))

  (add-hook 'eshell-mode-hook
            '(lambda () (define-key eshell-mode-map (kbd "C-=")  'helm-eshell-history)))

  (eval-after-load 'eshell
    '(require 'eshell-autojump nil t))

  (setq eshell-last-dir-ring-size 500)

  )
