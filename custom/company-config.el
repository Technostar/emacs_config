(use-package company
  :ensure t

  :init
  (add-hook 'after-init-hook 'global-company-mode)

  :config
  (add-to-list 'company-backends 'company-jedi   )
  (add-to-list 'company-backends 'company-ansible)
  )

(use-package company-emoji   :ensure t)
(use-package company-ansible :ensure t)

(use-package company-jedi  :ensure t :after jedi)
