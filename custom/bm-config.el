(use-package bm
  :ensure t

  :config
  (setq bm-cycle-all-buffers t)

  :bind (
         ("<f7>"      . bm-toggle  )
         ("<C-next>"  . bm-next    )
         ("<C-prior>" . bm-previous)))

(provide 'bm-config)
