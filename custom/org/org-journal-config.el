(defun journal-file-today ()
  (interactive)
  (let ((daily-name (format-time-string "%Y%m%d")))
    (find-file (concat org-journal-dir daily-name))))

(defun journal-file-yesterday ()
  (interactive)
  (let ((daily-name
         (format-time-string
          "%Y%m%d"
          (time-subtract (current-time) (days-to-time 1)))))
    (find-file  (concat org-journal-dir daily-name))))

(use-package org-journal
  :init
  (setq org-journal-date-format "#+TITLE: Journal Entry- %Y-%m-%d (%A)")
  (setq org-journal-dir         (expand-file-name "~/Documents/Org/Journals/")))

(provide 'org-journal-config)
