(provide 'org-blog-generation-scripts)

(defun org-export-pre-generation ()
  (let* ((base-directory (plist-get project-plist :base-directory))
         (export-directory (plist-get project-plist :publishing-directory))
         (index_file (expand-file-name "theindex.org" base-directory)))

    (org-publish-index-generate-theindex
     (org-publish-get-project-from-filename index_file)
     base-directory)

    (shell-command
     (format "%s/scripts/org-export-pre-generation.sh %s %s"
             base-directory base-directory export-directory))))

(defun org-export-post-generation ()
  (let* ((base-directory (plist-get project-plist :base-directory))
         (export-directory (plist-get project-plist :publishing-directory))
         (index_file (expand-file-name "theindex.org" base-directory)))

    (shell-command
     (format
      "%s/scripts/org-export-post-generation.sh %s %s"
      base-directory base-directory export-directory))))

(defun org-export-generation (project-plist data_file)
  (let* ((base-directory (plist-get project-plist :base-directory))
         (export-directory (plist-get project-plist :publishing-directory))
         (index_file (expand-file-name "theindex.org" base-directory)))

    (shell-command-to-string
     (format "%s/scripts/org-export-generation.sh %s %s %s"
             base-directory base-directory export-directory data_file))))
