(use-package async :ensure t)

(require 'org-blog-video-thumbnails  )
(require 'org-blog-youtube-thumbnails)

(defun mpv-play-media (media)
  (async-shell-command (format "mpv --quiet %s" media)))

(defun play-youtube-video-link (video_id)
  (message (format "playing youtube video %s" video_id))
  (mpv-play-media (format "https://www.youtube.com/watch?v=%s" video_id)))

(defun org-open-audio-link (&optional audio_file)
  (let ((q (or audio_file (org-at-special "audio:"))))
    (when q (org-player-play-file q)
          t)))

(defun org-open-youtube-link (&optional video_id)
  (let ((q (or video_id (org-at-special "youtube:"))))
    (when q (play-youtube-video-link q)
          t)))

(defun org-open-video-link (&optional video_id)
  (let ((q (or video_id (org-at-special "video:"))))
    (when q (mpv-play-media q)
          t)))

(defun export-audio-link (path desc backend &optional link)
  (cl-case backend
    (html (format
      "<div class='audio_container' align='center'>
           <audio controls>
              <source src='%s'>
           </audio>
           <p>%s</p>
       </div>"
      path (or (extract-link-caption link) (or desc ""))))))

(defun export-video-link (path desc backend &optional link)
  (cl-case backend
    (html (format
      "<div class='video_container' align='center'>
           <video width='%s' height='%s' controls>
               <source src='%s'>%s
           </video>
           <p>%s</p>
       </div>"
      (or (extract-link-extra-prop link 'width ) "700px")
      (or (extract-link-extra-prop link 'height) "400px")
      path (or desc "")
      (or (extract-link-caption link) (or desc ""))))))

(defun export-youtube-link (video_id desc backend &optional link)
  (cl-case backend
    (html (format
      "<div class='youtube_container' align='center'>
           <iframe width='%s' height='%s' src='https://www.youtube.com/embed/%s'
             frameborder='0' class='youtube-iframe' allowfullscreen>%s
           </iframe>
           <p>%s</p>
       </div>"
      (or (extract-link-extra-prop link 'width ) "700px")
      (or (extract-link-extra-prop link 'height) "400px")
      video_id (or desc "")
      (or (extract-link-caption link) "")))))

(defun extract-link-attributes (link extractor)
  (let ((paragraph
         (let ((e link))
           (while (and (setq e (org-element-property :parent e))
                       (not (eq (org-element-type e) 'paragraph))))
           e)))
    (when paragraph
      (save-excursion
        (goto-char (org-element-property :begin paragraph))
        (funcall extractor paragraph)))))

(defun extract-link-caption (link)
  (extract-link-attributes
   link (lambda (paragraph)
          (when (re-search-forward "^[ \t]*#\\+caption?: +\\(.+\\)"
                 (org-element-property :post-affiliated paragraph) t)
            (match-string 1)))))

(defun extract-link-extra-prop (link property)
  (extract-link-attributes
   link (lambda (paragraph)
          (when (re-search-forward (format "^[ \t]*#\\+attr_.*?: +.*?%s +\\(\\S-+\\)" property)
                 (org-element-property :post-affiliated paragraph) t)
            (match-string 1)))))

(defun custom-org-blog-media-config ()
  (if (version< (org-version) "9.0")
      (progn
        (org-add-link-type "audio"   #'org-open-audio-link   #'export-audio-link  )
        (org-add-link-type "video"   #'org-open-video-link   #'export-video-link  )
        (org-add-link-type "youtube" #'org-open-youtube-link #'export-youtube-link))
    (org-link-set-parameters "audio"   :follow #'org-open-audio-link   :export #'export-audio-link  )
    (org-link-set-parameters "video"   :follow #'org-open-video-link   :export #'export-video-link  )
    (org-link-set-parameters "youtube" :follow #'org-open-youtube-link :export #'export-youtube-link))

  (eval-after-load "ox-html"
    '(defun org-export-custom-protocol-maybe (link desc backend)
       (let ((type (org-element-property :type link)))
         (unless (or (member type '("coderef" "custom-id" "fuzzy" "radio"))
                     (not backend))
           (let ((protocol (org-link-get-parameter type :export)))
             (and (functionp protocol)
                  (funcall protocol
                           (org-link-unescape (org-element-property :path link))
                           desc backend link)))))))

  (add-hook
   'org-mode-hook
   (lambda ()
     (unless org-inhibit-startup
       (when org-startup-with-inline-youtube-thumbnails
         (org-display-inline-youtube-thumbnails))
       (when org-startup-with-inline-video-thumbnails
         (org-display-inline-video-thumbnails))))))

(provide 'org-blog-media-config)
