(defun org-config ()
  (require 'ob-ipython)

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((dot        . t)
     (sql        . t)
     (shell      . t)
     (ditaa      . t)
     (python     . t)
     (latex      . t)
     (ipython    . t)
     (haskell    . t)
     (plantuml   . t)
     (emacs-lisp . t)))

  (defadvice org-babel-python-evaluate-session
      (around org-python-use-cpaste (session body &optional result-type result-params) activate)
    "add a %cpaste and '--' to the body, so that ipython does the right thing."
    (setq body (concat "%cpaste -q \n" body "\n--"))
    ad-do-it)

  (defun org-babel-execute:ditaa (body params)
    "Overwriting the default or-babel-execute for the ditaa format, because fedora has a command for that"
    (let* ((result-params (split-string (or (cdr (assoc :results params)) "")))
           (out-file ((lambda (el) (or el (error "ditaa code block requires :file header argument")))
                      (cdr (assoc :file params))))
           (cmdline (cdr (assoc :cmdline params)))
           (in-file (org-babel-temp-file "ditaa-"))
           (cmd (concat "/usr/bin/ditaa" " " cmdline " " (org-babel-process-file-name in-file) " " (org-babel-process-file-name out-file))))
      (with-temp-file in-file (insert body))
      (message cmd) (shell-command cmd)
      nil))

  (defun org-babel-execute-and-show-images ()
    (interactive)
    (org-babel-execute-src-block)
    (org-display-inline-images t t))

  (setq org-todo-keywords
        '((sequence "TODO(t)" "NEXT(n)" "WAITING(w@/!)" "IN PROGRESS(p)" "BUGGY(g@/)" "BROKEN(k@/)"
                    "|"
                    "CANCELLED(c@/!)" "DONE(d)")))

  (setq org-priority-faces '((65 . "DeepPink") (66 . "firebrick") (67 . "tomato")))

  (setq-default ob-ipython-command              "ipython3"               )

  (setq org-todo-keyword-faces
         '(("TODO"        :foreground "red"          :weight bold)
           ("NEXT"        :foreground "blue"         :weight bold)
           ("DONE"        :foreground "forest green" :weight bold)
           ("WAITING"     :foreground "orange"       :weight bold)
           ("IN PROGRESS" :foreground "orange"       :weight bold)
           ("BUGGY"       :foreground "goldnrod"     :weight bold)
           ("BROKEN"      :background "black"        :foreground "dark red" :weight bold)
           ("CANCELLED"   :foreground "forest green" :weight bold)
           ))

  (defun surround (start end txt)
    (interactive "r\nsEnter text to surround: " start end txt)
    (if (not (region-active-p))
        (let ((new-region (bounds-of-thing-at-point 'symbol)))
          (setq start (car new-region))
          (setq end (cdr new-region))))
    (let* ((s-table '(("#e" . ("#+BEGIN_EXAMPLE\n" "\n#+END_EXAMPLE") )
                      ("#s" . ("#+BEGIN_SRC \n"    "\n#+END_SRC") )
                      ("#q" . ("#+BEGIN_QUOTE\n"   "\n#+END_QUOTE"))
                      ("<"  . ("<" ">"))
                      ("("  . ("(" ")"))
                      ("{"  . ("{" "}"))
                      ("["  . ("[" "]"))))
           (s-pair (assoc-default txt s-table)))
      (unless s-pair (setq s-pair (list txt txt)))

      (save-excursion
        (narrow-to-region start end)
        (goto-char (point-min))
        (insert (car s-pair))
        (goto-char (point-max))
        (insert (cadr s-pair))
        (widen))))

  (defun surround-text (txt)
    (if (region-active-p)
        (surround (region-beginning) (region-end) txt)
      (surround nil nil txt)))

  (defun org-text-bold    () (interactive) (surround-text "*"))
  (defun org-text-code    () (interactive) (surround-text "="))
  (defun org-text-italics () (interactive) (surround-text "/"))

  (setq org-babel-python-command "ipython --no-banner --classic --no-confirm-exit --colors Linux --no-autoindent")

  (define-key org-mode-map (kbd "<M-tab>") 'yas-expand)

  (font-lock-add-keywords 'org-mode
                          '(("^ +\\([-*]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "☢"))))))

  (add-to-list 'ispell-skip-region-alist '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:"))
  (add-to-list 'ispell-skip-region-alist '("#\\+BEGIN_SRC" . "#\\+END_SRC"))

  (let* ((headline `(:inherit default :weight bold)))
    (custom-theme-set-faces 'user
                            `(org-level-8 ((t (,@headline            ))))
                            `(org-level-7 ((t (,@headline            ))))
                            `(org-level-6 ((t (,@headline            ))))
                            `(org-level-5 ((t (,@headline            ))))
                            `(org-level-4 ((t (,@headline            ))))
                            `(org-level-3 ((t (,@headline :height 1.2))))
                            `(org-level-2 ((t (,@headline :height 1.5))))
                            `(org-level-1 ((t (,@headline :height 2.0))))
                            `(org-document-title ((t (,@headline :height 1.5 :underline nil))))))

  (custom-set-faces
   '(org-code           ((t (:background "RoyalBlue2" :foreground "yellow"           ))))
   '(org-level-1        ((t (:inherit default :weight bold :height 2.0               ))))
   '(org-level-2        ((t (:inherit default :weight bold :height 1.5               ))))
   '(org-level-3        ((t (:inherit default :weight bold :height 1.2               ))))
   '(org-level-4        ((t (:inherit default :weight bold                           ))))
   '(org-level-5        ((t (:inherit default :weight bold                           ))))
   '(org-level-6        ((t (:inherit default :weight bold                           ))))
   '(org-level-7        ((t (:inherit default :weight bold                           ))))
   '(org-level-8        ((t (:inherit default :weight bold                           ))))
   '(org-document-title ((t (:inherit default :weight bold :height 1.5 :underline nil))))
   )

  (setq org-clock-clocktable-default-properties (quote (:maxlevel nil :scope file)))
  (setq org-enforce-todo-checkbox-dependencies t)
  (setq org-enforce-todo-dependencies t)
  (setq org-log-into-drawer t)
  (setq org-log-reschedule (quote note))
  (setq org-modules
        '(org-bbdb org-bibtex org-crypt org-ctags org-docview
          org-habit org-info org-irc org-mhe org-protocol org-w3m))

  (setq org-refile-targets '((nil :maxlevel . 9) (org-agenda-files :maxlevel . 9)))
  (setq org-refile-use-outline-path t)
  (setq org-outline-path-complete-in-steps nil)

  (define-key org-mode-map (kbd "<f6>"        ) 'flyspell-buffer)
  (define-key org-mode-map (kbd "<C-M-return>") (lambda() (interactive) (insert-char 8626 1 t)))
  (define-key org-mode-map (kbd "M-é"         ) 'org-refile)

  (key-chord-define org-mode-map "ww" 'org-todo                         )
  (key-chord-define org-mode-map ">>" 'org-edit-special                 )
  (key-chord-define org-mode-map "!!" 'helm-flyspell-correct            )
  (key-chord-define org-mode-map "::" 'flyspell-goto-next-error         )
  (key-chord-define org-mode-map "xx" 'org-toggle-inline-images         )
  (key-chord-define org-mode-map "<<" 'org-babel-execute-and-show-images)

  (setq org-latex-pdf-process '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
  (setq org-agenda-file-regexp  "\\`[^.].*\\.org\\'")

  (setq TeX-PDF-mode                            t                                              )
  (setq org-log-done                            'time                                          )
  (setq org-plantuml-jar-path                   "/usr/share/java/plantuml.jar"                 )
  (setq org-image-actual-width                  nil                                            )
  (setq org-src-fontify-natively                t                                              )
  (setq org-export-latex-listings               'minted                                        )
  (setq org-hide-emphasis-markers               t                                              )
  (setq org-confirm-babel-evaluate              nil                                            )
  (setq org-list-allow-alphabetical             t                                              )
  (setq org-fontify-emphasized-text             t                                              )
  (setq-default org-hide-leading-stars          t                                              )
  (setq-default org-support-shift-select        (quote always)                                 )
  (setq-default org-fontify-done-headline       t                                              )
  (setq-default org-fontify-whole-heading-line  t                                              )
  (setq-default org-highlight-latex-and-related (quote (latex script entities))                )

  (add-to-list 'auto-mode-alist '(".*/[0-9]*$" . org-mode))

  (org-clock-persistence-insinuate)
  )

(use-package htmlize       :ensure t)
(use-package ob-ipython    :ensure t)
(use-package org-journal   :ensure t)
(use-package ob-restclient :ensure t)

(add-hook 'kill-emacs-hook (lambda ()
                               (require 'org-crypt)
                               (org-encrypt-entries)
                               (org-save-all-org-buffers)))

(add-hook 'org-mode-hook (lambda ()
                             (auto-fill-mode           )
                             (elpy-use-cpython         )
                             (visual-line-mode         )
                             (org-bullets-mode 1       )
                             (org-display-inline-images)))

(setq org-directory                           (expand-file-name "~/Documents/Org/"          ))

(provide 'org-config)
