(use-package cython-mode
  :mode ("\\.pyx\\'" . cython-mode)
  :ensure t
  :config
  (flycheck-mode t))

(use-package python
  :mode ("\\.py\\'" . python-mode)
  :ensure t
  :interpreter ("python3" . python-mode)
  :init
  (add-hook 'python-mode-hook 'elpy-mode    )
  (add-hook 'python-mode-hook 'hs-minor-mode)

  :config
  (use-package elpy
  :after python
  :ensure t

  :init
  (add-hook 'elpy-mode-hook 'flycheck-mode)
  (when (not (getenv "WORKON_HOME"))
    (setenv "WORKON_HOME" "~/.cache/virtualenv"))

  :config
  (setq elpy-rpc-backend                "jedi"             )
  (setq elpy-test-django-runner         'elpy-test-runner-p)
  (setq elpy-rpc-large-buffer-size      40960              )
  (setq elpy-interactive-python-command "ipython3"         )

  (elpy-enable             )
  (pyvenv-mode      t      )
  (pyvenv-workon    "emacs")
  (smartparens-mode t      )

  (key-chord-define python-mode-map ";;" 'sphinx-doc                           )
  (key-chord-define python-mode-map "vv" 'pyvenv-workon                        )
  (key-chord-define python-mode-map "!!" 'elpy-find-file                       )
  (key-chord-define python-mode-map "wq" 'elpy-multiedit-python-symbol-at-point)

  (when (require 'sphinx-doc nil t)
    (sphinx-doc-mode  t ))
  (when (require 'flycheck nil t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules)))

  (defadvice elpy-shell-get-or-create-process (before force-custom-interpreter activate)
    (setf elpy-rpc-python-command         "python3" )
    (setq python-shell-interpreter        "ipython3"
          python-shell-interpreter-args "-i --simple-prompt")
    (setq elpy-interactive-python-command "ipython3"))

  :bind (
         :map python-mode-map
              ("<C-M-return>" . elpy-goto-definition)
              ("C-s-²"        . elpy-rpc-restart)
              ("<M-RET>"      . elpy-multiedit-python-symbol-at-point))))


(use-package jedi            :ensure t :defer t          )
(use-package flycheck-cython :ensure t :after cython-mode)

(provide 'python-config)
