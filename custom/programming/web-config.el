(provide 'web-config)

(use-package web-mode
  :ensure t

  :config
  (use-package emmet-mode :ensure t)
  (require 'flycheck  )
  (require 'key-chord )

  (defun custom-web-mode-hook ()
    (emmet-mode)
    (flycheck-mode)
    (electric-pair-mode nil)

    (setq web-mode-block-padding                    t)
    (setq web-mode-style-padding                    4)
    (setq web-mode-script-padding                   4)
    (setq web-mode-auto-close-style                 2)
    (setq web-mode-enable-auto-expanding            t)
    (setq web-mode-enable-current-element-highlight t)

    (setq js2-highlight-level 3)
    (setq js2-mode-show-strict-warnings nil)

    (setq js-indent-level 4)
    (setq js2-ignored-warnings (quote ("msg.missing.semi")))

    (setq web-mode-engines-alist '(("mako"   . "\\.html\\'")
                                   ("django" . "templates/.*\\.html\\'")))

    (setq web-mode-extra-expanders
          '(("L/" . "<label for=\"\">|</label>")
            ("I/" . "<input type=\"|\"></input>")))

    (setq web-mode-extra-snippets
          '(("django" . (("trans"  . "{% trans '|' %}")
                         ("btrans" . "{% blocktrans %}\n|\n{% endblocktrans %}")))))
    )

  (defun custom-js2-mode-hook ()
    (flycheck-mode)
    (flycheck-add-mode 'javascript-eslint 'js2-mode)
    (prettify-symbols-mode nil))

  (defun custom-js-mode-hook ()
    (flycheck-mode)
    (flycheck-add-mode 'javascript-eslint 'js-mode)
    (prettify-symbols-mode nil))

  (defun use-eslint-from-node-modules ()
    (let* ((root (locate-dominating-file
                  (or (buffer-file-name) default-directory)
                  "node_modules"))
           (eslint (and root
                        (expand-file-name "node_modules/eslint/bin/eslint.js"
                                          root))))
      (when (and eslint (file-executable-p eslint))
        (setq-local flycheck-javascript-eslint-executable eslint))))

  (add-hook 'flycheck-mode-hook #'use-eslint-from-node-modules)

  (with-eval-after-load 'flycheck
    (flycheck-add-mode 'javascript-eslint 'web-mode)
    (flycheck-add-mode 'html-tidy         'web-mode)
    (flycheck-add-mode 'css-csslint       'web-mode))

  (add-hook 'web-mode-hook       'custom-web-mode-hook)
  (add-hook 'js2-mode-hook       'custom-js2-mode-hook)
  (add-hook 'js2-minor-mode-hook 'custom-js2-mode-hook)
  (add-hook 'js-mode-hook        'custom-js-mode-hook )

  (key-chord-define web-mode-map "wq" 'web-mode-snippet-insert)

  (define-key web-mode-map (kbd "<C-tab>") 'web-mode-fold-or-unfold)
  (define-key web-mode-map (kbd "<M-tab>") 'web-mode-snippet-insert)
  (define-key web-mode-map (kbd "<C-return>") 'emmet-expand-line)

  (add-to-list 'auto-mode-alist '("\\.js\\'"   . js2-mode))
  (add-to-list 'auto-mode-alist '("\\.css\\'"  . web-mode))
  (add-to-list 'auto-mode-alist '("\\.hbs\\'"  . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.vue\\'"  . web-mode))

  (custom-set-faces
   '(web-mode-html-tag-face         ((t (:foreground "cornflower blue"))))
   '(web-mode-block-control-face    ((t (:foreground "lime green"     ))))
   '(web-mode-html-attr-name-face   ((t (:foreground "dark orange"    ))))
   '(web-mode-block-delimiter-face  ((t (:foreground "forest green"   ))))
   '(web-mode-html-attr-custom-face ((t (:foreground "firebrick"      ))))
   '(web-mode-html-tag-bracket-face ((t (:foreground "cornflower blue"))))
   )

  ;; disable jshint since we prefer eslint checking
  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(javascript-jshint)))

)
