(require 'package)
(setq package-enable-at-startup nil)

(add-to-list 'package-archives '("org"   . "http://orgmode.org/elpa/"           )t)
(add-to-list 'package-archives '("elpa"  . "http://elpa.gnu.org/packages/"      )t)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/" )t)

(progn (cd "~/.emacs.d") (normal-top-level-add-subdirs-to-load-path))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(use-package key-chord
  :ensure t
  :config
  (key-chord-mode 1))

;; Misc functions collections
(require 'lines-utils         )
(require 'buffers-utils       )
(require 'toggle-split-utils  )
(require 'misc-functions-utils)

;; Org configuration
(require 'org-config           )
(require 'org-blog-config      )
(require 'org-agenda-config    )
(require 'org-journal-config   )
(require 'org-blog-media-config)

(org-config     )
(org-blog-config)

;; Packages config
(require 'bm-config                )
(require 'cpp-config               )
(require 'web-config               )
(require 'emms-config              )
(require 'yaml-config              )
(require 'helm-config              )
(require 'hydra-config             )
(require 'slack-config             )
(require 'jabber-config            )
(require 'python-config            )
(require 'scheme-config            )
(require 'paredit-config           )
(require 'haskell-config           )
(require 'desktop-config           )
(require 'ansible-config           )
(require 'pomodoro-config          )
(require 'undo-tree-config         )
(require 'powerline-config         )
(require 'space-line-config        )
(require 'projectile-config        )
(require 'persistent-scratch-config)

(use-package magit            :ensure t)
(use-package calfw            :ensure t)
(use-package company          :ensure t)
(use-package hcl-mode         :ensure t)
(use-package json-mode        :ensure t)
(use-package calfw-org        :ensure t)
(use-package calfw-cal        :ensure t)
(use-package yatemplate       :ensure t)
(use-package calfw-gcal       :ensure t)
(use-package calfw-ical       :ensure t)
(use-package idris-mode       :ensure t)
(use-package helm-idris       :ensure t)
(use-package nhexl-mode       :ensure t)
(use-package google-this      :ensure t)
(use-package unicode-fonts    :ensure t)
(use-package gruvbox-theme    :ensure t)
(use-package multiple-cursors :ensure t)

(use-package whitespace-cleanup-mode
    :ensure t
    :diminish whitespace-cleanup-mode
    :init
    (setq whitespace-cleanup-mode-only-if-initially-clean nil
          whitespace-line-column 80
          whitespace-style '(face lines-tail))
    :config
    (global-whitespace-mode)
    (global-whitespace-cleanup-mode))


(use-package imenu-list
  :ensure t
  :config
  (setq imenu-list-auto-resize t    )
  (setq imenu-list-position    'left)
  :bind (("C-'" . imenu-list-smart-toggle)))

(use-package eyebrowse
  :ensure t
  :config
  (setq eyebrowse-new-workspace t)
  (eyebrowse-mode 1))

(use-package smart-tabs-mode
  :ensure t
  :config
  (smart-tabs-insinuate 'c))

;; (use-package fill-column-indicator
;;   :ensure t
;;   :config
;;   (setq fci-rule-width 4     )
;;   (setq fci-rule-color "grey")

;;   (define-globalized-minor-mode global-fci-mode fci-mode
;;     (lambda ()
;;       (if (and
;;            (not (string-match "^\*.*\*$" (buffer-name)))
;;            (not (eq major-mode 'dired-mode)))
;;           (fci-mode 1))))
;;   (global-fci-mode 1))

(use-package popwin
  :ensure t
  :config
  (popwin-mode 1))

;; Custom configurations (migration to use-package pending)
(require 'custom-tramp-config      )
(require 'custom-eshell-config     )
(require 'custom-shortcuts-config  )
(require 'custom-smartparens-config)

(custom-tramp-config    )
(custom-eshell-config   )
(custom-shortcuts-config)


(add-hook 'elm-mode-hook        #'elm-oracle-setup-completion        )
(add-hook 'term-mode-hook       (lambda() (setq yas-dont-activate t))     )
(add-hook 'after-init-hook      #'global-emojify-mode                )
(add-hook 'before-save-hook     'delete-trailing-whitespace          )
(add-hook 'jabber-activity-mode 'emoji-cheat-sheet-plus-display-mode )

(add-to-list 'auto-mode-alist      '("\\.rst$" . rst-mode )                                )
(add-to-list 'auto-mode-alist      '("/mutt"   . mail-mode)                                )
(add-to-list 'display-buffer-alist '("^*Async Shell Command*" . (display-buffer-no-window)))

(auto-insert-mode )
(ac-config-default)

(yatemplate-fill-alist )
(global-undo-tree-mode )

(hl-line-mode           1 )
(winner-mode            1 )
(menu-bar-mode          -1)
(tool-bar-mode          -1)
(yas-global-mode        1 )
(show-paren-mode        1 )
(scroll-bar-mode        -1)
(global-linum-mode      t )
(delete-selection-mode  1 )
(column-number-mode     1 )
(transient-mark-mode    1 )
(global-font-lock-mode  1 )
(global-whitespace-mode 1 )

(ac-set-trigger-key   "<tab>"   )
(load-theme 'gruvbox-dark-hard t)

(setq whitespace-style (quote (face spaces tabs newline space-mark tab-mark)))
(setq whitespace-display-mappings
    '((space-mark   32 [183    ] [46  ])
      (newline-mark 10 [8629 10]       )
      (tab-mark      9 [8594  9] [92 9])))

(setq fci-rule-color               "lightgray"                                        )
(setq fci-rule-column              100                                                )
(setq yas/indent-line              nil                                                )
(setq yas-snippet-dirs             '("~/.emacs.d/snippets" yas-installed-snippets-dir))
(setq backup-inhibited             t                                                  )
(setq dired-dwim-target            t                                                  )
(setq inhibit-splash-screen        t                                                  )
(setq inferior-lisp-program        "/usr/bin/sbcl"                                    )
(setq whitespace-line-column       100                                                )
(setq inhibit-startup-message      t                                                  )
(setq initial-scratch-message      nil                                                )
(setq enable-recursive-minibuffers t                                                  )

(setq-default tab-width        4  )
(setq-default fill-column      100)
(setq-default truncate-lines   t  )
(setq-default indent-tabs-mode nil)

(put 'scroll-left               'disabled nil)
(put 'upcase-region             'disabled nil)
(put 'downcase-region           'disabled nil)
(put 'narrow-to-page            'disabled nil)
(put 'narrow-to-region          'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

(defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
  (cl-letf (((symbol-function #'process-list) (lambda ())))
    ad-do-it))

(eval-after-load "linum"
  '(set-face-attribute 'linum nil :height 120))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "monoOne" :slant normal :height 120 :width normal :weight normal))))
 '(bm-face ((t (:background "DarkSeaGreen4" :foreground "snow1" :weight bold))))
 '(org-code ((t (:background "RoyalBlue2" :foreground "yellow"))))
 '(org-document-title ((t (:inherit default :weight bold :height 1.5 :underline nil))))
 '(org-habit-alert-face ((t (:background "gold" :foreground "black"))))
 '(org-habit-alert-future-face ((t (:background "darkgoldenrod" :foreground "black"))))
 '(org-level-1 ((t (:inherit default :weight bold :height 2.0))))
 '(org-level-2 ((t (:inherit default :weight bold :height 1.5))))
 '(org-level-3 ((t (:inherit default :weight bold :height 1.2))))
 '(org-level-4 ((t (:inherit default :weight bold))))
 '(org-level-5 ((t (:inherit default :weight bold))))
 '(org-level-6 ((t (:inherit default :weight bold))))
 '(org-level-7 ((t (:inherit default :weight bold))))
 '(org-level-8 ((t (:inherit default :weight bold))))
 '(spaceline-flycheck-error ((t (:foreground "dark red" :weight bold))))
 '(spaceline-flycheck-info ((t (:foreground "RoyalBlue4" :weight bold))))
 '(spaceline-flycheck-warning ((t (:foreground "purple4" :weight bold))))
 '(web-mode-block-control-face ((t (:foreground "lime green"))))
 '(web-mode-block-delimiter-face ((t (:foreground "forest green"))))
 '(web-mode-html-attr-custom-face ((t (:foreground "firebrick"))))
 '(web-mode-html-attr-name-face ((t (:foreground "dark orange"))))
 '(web-mode-html-tag-bracket-face ((t (:foreground "cornflower blue"))))
 '(web-mode-html-tag-face ((t (:foreground "cornflower blue"))))
 '(whitespace-empty ((t (:foreground "firebrick"))))
 '(whitespace-hspace ((t (:foreground "lightgray"))))
 '(whitespace-indentation ((t (:foreground "#404040"))))
 '(whitespace-line ((t (:foreground "firebrick"))))
 '(whitespace-newline ((t (:foreground "orange"))))
 '(whitespace-space ((t (:foreground "#404040"))))
 '(whitespace-space-after-tab ((t (:foreground "firebrick"))))
 '(whitespace-space-before-tab ((t (:foreground "firebrick"))))
 '(whitespace-tab ((t (:foreground "#404040"))))
 '(whitespace-trailing ((t (:foreground "yellow")))))

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (paredit-mode)
            (prettify-symbols-mode)))

(add-hook 'align-load-hook (
  lambda () (add-to-list 'align-rules-list '(python-imports
                                        (regexp  . "\\(\\s-+\\)import")
                                        (group   . 1)))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-modes
   (quote
    (emacs-lisp-mode lisp-mode lisp-interaction-mode slime-repl-mode nim-mode c-mode cc-mode c++-mode go-mode java-mode malabar-mode clojure-mode clojurescript-mode scala-mode scheme-mode ocaml-mode tuareg-mode coq-mode haskell-mode agda-mode agda2-mode perl-mode cperl-mode ruby-mode lua-mode tcl-mode ecmascript-mode javascript-mode js-mode js-jsx-mode js2-mode js2-jsx-mode php-mode css-mode scss-mode less-css-mode makefile-mode sh-mode fortran-mode f90-mode ada-mode xml-mode sgml-mode web-mode ts-mode sclang-mode verilog-mode qml-mode apples-mode)))
 '(ansi-color-names-vector
   ["#454545" "#cd5542" "#6aaf50" "#baba36" "#5180b3" "#ab75c3" "#68a5e9" "#bdbdb3"])
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(browse-url-browser-function (quote browse-url-firefox))
 '(custom-safe-themes
   (quote
    ("cd4d1a0656fee24dc062b997f54d6f9b7da8f6dc8053ac858f15820f9a04a679" "62c81ae32320ceff5228edceeaa6895c029cc8f43c8c98a023f91b5b339d633f" "a5956ec25b719bf325e847864e16578c61d8af3e8a3d95f60f9040d02497e408" "f27c3fcfb19bf38892bc6e72d0046af7a1ded81f54435f9d4d09b3bff9c52fc1" "938d8c186c4cb9ec4a8d8bc159285e0d0f07bad46edf20aa469a89d0d2a586ea" default)))
 '(desktop-save t)
 '(display-time-24hr-format t)
 '(display-time-day-and-date t)
 '(display-time-mode t)
 '(elfeed-feeds (quote ("http://planet.emacsen.org/atom.xml")))
 '(mail-user-agent (quote sendmail-user-agent))
 '(menu-bar-mode nil)
 '(mm-encrypt-option nil)
 '(mm-sign-option nil)
 '(mode-require-final-newline nil)
 '(package-selected-packages
   (quote
    (fish-completion fish-mode whitespace-cleanup-mode google-this helm-google htmlize magit multiple-cursors helm-projectile gruvbox-theme yaml-mode use-package undo-tree spaceline smart-tabs-mode slack projectile popwin pomodoro persistent-scratch ob-restclient ob-ipython nhexl-mode key-chord json-mode jedi jabber intero imenu-list hydra helm-rtags helm-idris helm-hoogle helm-dash helm-ag hcl-mode hasky-stack hasky-extensions haskell-snippets flycheck-irony flycheck-haskell flycheck-cython fill-column-indicator eyebrowse elpy cython-mode company-rtags company-irony calfw-org calfw-ical calfw-gcal calfw-cal calfw bm ansible-doc ansible)))
 '(powerline-gui-use-vcs-glyph t)
 '(projectile-project-root-files
   (quote
    (".projectile" ".git" "rebar.config" "project.clj" "build.boot" "SConstruct" "pom.xml" "build.sbt" "gradlew" "build.gradle" ".ensime" "Gemfile" "requirements.txt" "setup.py" "tox.ini" "composer.json" "Cargo.toml" "mix.exs" "stack.yaml" "info.rkt" "DESCRIPTION" "TAGS" "GTAGS")))
 '(projectile-project-root-files-top-down-recurring (quote (".projectile" ".svn" "CVS" "Makefile")))
 '(python-indent-guess-indent-offset nil)
 '(python-shell-completion-native-disabled-interpreters (quote ("pypy" "ipython" "python3")))
 '(safe-local-variable-values
   (quote
    ((org-agenda-files ".")
     (buffer-file-coding-system . utf-8-unix)
     (haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4)
     (eval org-babel-lob-ingest "../lob/general.lob")
     (org-html-metadata-timestamp-format . "%Y-%m-%d")
     (org-html-postamble-format . %T)
     (org-html-validation-link))))
 '(send-mail-function (quote smtpmail-send-it))
 '(shell-pop-autocd-to-working-dir nil)
 '(shell-pop-full-span t)
 '(shell-pop-shell-type
   (quote
    ("ansi-term" "*ansi-term*"
     (lambda nil
       (ansi-term shell-pop-term-shell)))))
 '(shell-pop-universal-key "<f12>")
 '(shell-pop-window-position "full")
 '(show-paren-mode t)
 '(smtpmail-default-smtp-server "smtp.gmail.com")
 '(smtpmail-queue-dir "~/Documents/mail/queued-mail/")
 '(smtpmail-queue-mail t)
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 465)
 '(smtpmail-smtp-user "chedi.toueiti@gmail.com")
 '(smtpmail-stream-type (quote ssl))
 '(user-full-name "chedi toueiti")
 '(user-mail-address "chedi.toueiti@gmail.com"))

(autoload 'enable-paredit-mode                   "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook                  #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook                        #'enable-paredit-mode)
(add-hook 'lisp-mode-hook                        #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook            #'enable-paredit-mode)
(add-hook 'scheme-mode-hook                      #'enable-paredit-mode)

(setq byte-compile-warnings '(not callargs redefine obsolete nresolved free-vars
                                  noruntime cl-functions interactive-only))
